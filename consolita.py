import requests as req

opcion = -1

while(opcion!="0"):
	print("=============== RECOMENDADOR ANIMES ===============")
	opcion = input("Elige en base a qué quieres las recomendaciones:\n1. Usuario\n2. Lista de animes\n0. Salir\n\n")

	if opcion == "1":
		usuario = input("Introduce el número de usuario:\n")
		res = req.get("http://dqueralt.pythonanywhere.com/recomendacion_animeU?usuario=" + usuario)
		data = res.json()
		print(data)

	elif opcion == "2":
		longitud = int(input("¿Cuántos animes vas a introducir?\n"))
		url = "http://dqueralt.pythonanywhere.com/recomendacion_animeL?animes="

		for anime in range(0,longitud):
			anime_introducido = input("Introduce título de anime:\n")
			puntuacion_introducida = input("Introduce la puntuación del anime:\n")
			
			url = url + anime_introducido + "|" + puntuacion_introducida + ";"
		
		url = url[0:len(url)-1]

		res = req.get(url)
		data = res.json()
		print(data)		

	else:
		print("Opción no válida")