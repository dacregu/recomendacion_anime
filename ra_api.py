#Importar flask, jsonify, request
from flask import Flask, jsonify, request as req
#import pandas as pd
#Importar clase recomendacion_anime
from recomendacion_anime import *
#Generar la app de flask
app = Flask(__name__)

#Generamos el objeto de recomendaciones
recomendaciones = Recomendaciones()

#Ruta para entrenar el algoritmo
@app.route("/trainRecomendaciones", methods=["POST"])
#Función para entrenar el algotimo
def trainRecomendaciones():
    recomendaciones.crear_corrMatrix()
    return jsonify({"OK":"OK"})

#Ruta para recomendar animes por usuario
@app.route("/recomendacion_animeU", methods=["GET"])
#Función para recomendar anime por usuario
def recomendarAnimeU():
    usuario = req.args.get('usuario')
    animes = []
    try: 
        recomendaciones.setMyRatingByUser(usuario)
    
    except Exception:
        return jsonify({"error" : "Usuario no encontrado"})

    ra = recomendaciones.myRatings()

    #Crea una lista de titulo y puntuación por todos valores de las recomendaciones del usuario
    for key, value in ra.items():
        animes.append({"titulo": key, "puntuacion": value})

    #Devuelve json con lista de animes recomendados
    return jsonify({"animes": animes}) 

#Ruta para recomendar animes por lista de animes y puntuación
@app.route("/recomendacion_animeL", methods=["GET"])
#Función para recomendar animes por lista de animes y puntuación
def recomendarAnimeL():
    animeList = req.args.get('animes')
    animeDict = {}
    animeList=animeList.split(';')
    animes = []
    
    #Recorre la lista de animes y separamos nombre y puntuación, se guarda en diccionario
    for anime in animeList:
        splitAnime = anime.split('|')
        nombre_anime = splitAnime[0]
        puntuacion = splitAnime[1]
        animesDict[nombre_anime] = puntuacion

    recomendaciones.setMyRatingByAnimeList(animeDict)

    #Crea lista de recomendaciones a partir de diccionario de animes
    ra = recomendaciones.myRatings()

    #Crea matriz de titulo y puntuación de las recomendaciones
    for key, value in ra.items():
        animes.append({"titulo": key, "puntuacion": value})

    #Devuelve jason
    return jsonify({"animes": animes}) 

