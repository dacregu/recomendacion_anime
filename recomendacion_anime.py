#Se importa la librería pandas
import pandas as pd
import json
#Creamos la clase recomendaciones
class Recomendaciones():
#Constructor
    def __init__(self):
        self._rutaHome = "home/dqueralt/recomendacion_anime"

    #Función para crear la matriz de correlación 
    def crear_corrMatrix(self):
        #Leemos los archivos
        animes = pd.read_csv(self._rutaHome+'/datos/anime.csv', sep=',')
        ratings = pd.read_csv(self._rutaHome+'/datos/rating.csv', sep=',', usecols=range(3))
        #Eliminamos las columnas innecesarias
        del animes['genre']
        del animes['type']
        del animes['episodes']
        del animes['rating']
        del animes['members']
        #Eliminamos los ratings -1 (no puntuado)
        ratings = ratings[ratings.rating != -1]
        #Unimos las dos matrices
        ratings = pd.merge(animes, ratings)
        #Creamos pivot table de 1 millón de datos de la tabla ratings
        animeRatings = ratings[:1000000].pivot_table(index=['user_id'],columns=['name'],values='rating')
        #Creamos la matriz de correlación entre pelis con método Pearson
        corrMatrix = animeRatings.corr(method='pearson', min_periods=100)
        
        #Guardamos la correlacion en un JSON
        corrMatrix = corrMatrix.to_json(self._rutaHome+'\\datos\\corrMatrix.json',orient='index')
        animeRatings = animeRatings.to_json(self._rutaHome+'\\datos\\animeRatings.json',orient='index')
    
    #Función para obtener ratings por user ID
    def setMyRatingByUser(self, usuario):
        #Recogemos la lista de "animeRatings" en base a JSON
        animeRatings = None
        # reading the JSON data using json.load()
        file = self._rutaHome+'\\datos\\animeRatings.json'
        with open(file) as train_file:
            dictFile = json.load(train_file)

        # converting json dataset from dictionary to dataframe
        animeRatings = pd.DataFrame.from_dict(dictFile, orient='index')

        #Se calculan los ratings por un usuario ID que se le pasa a la función (y se eliminan valores NaN)
        self._myRatings = animeRatings.loc[usuario].dropna()

    #Función para obtener ratings por título de anime y puntuación   
    def setMyRatingByAnimeList(self, animeDict):
        self._myRatings = animeDict
    
    #Función genérica para obtener los ratings
    def myRatings(self):
        simCandidates = pd.Series()
        #Recogemos lo datos del arcgivo JSON
        corrMatrix = None
        # reading the JSON data using json.load()
        file = self._rutaHome+'\\datos\\corrMatrix.json'
        with open(file) as train_file:
            dictFile = json.load(train_file)

        # converting json dataset from dictionary to dataframe
        corrMatrix = pd.DataFrame.from_dict(dictFile, orient='index')

        #Recorremos la lista de ratings y seleccionamos aquellos animes con puntuación mayor a 7
        for anime, puntuacion in self._myRatings.items():
            if puntuacion > 7:
            # Recuperar las pelis similares a las calificadas
                sims = corrMatrix[anime].dropna()
            # Escalar la similaridad multiplicando por la calificación de la persona
                sims = sims.map(lambda x: x * puntuacion)
            # Añadir el puntaje a la lista de candidatos similares
                simCandidates = simCandidates.append(sims)            

        #Agrupamos los items similares y sumamos los valores de los ratings
        simCandidates = simCandidates.groupby(simCandidates.index).sum()
        #Ordenamos los items de mayor a menor nota
        simCandidates.sort_values(inplace = True, ascending = False)
        #Solo devolvemos los diez primeros resultados
        return simCandidates.head(10)